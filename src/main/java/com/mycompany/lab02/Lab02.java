/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab02;

import java.util.Scanner;

/**
 *
 * @author krnis
 */
public class Lab02 {
    
    static char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    static char currentPlayer = 'X';
    static int row = 0;
    static int col = 0;
    static int count = 0;

    
    static void printWelcome(){
        System.out.println("Welcome To XOXO GAME!!");
    }
    
    static void printTable(){
        System.out.println("---------------");
        for(int i=0; i<3; i++){
            System.out.print(" |");
            for(int j=0; j<3; j++){
                System.out.print(" "+table[i][j]+" |");
            }
            System.out.println();
        }
        System.out.println("---------------");
    }

    static void printTurn(){
        System.out.print("Player "+currentPlayer+" turn");
        
    }
    
    static void printMove(){
        System.out.print(", Please enter row,col : ");
        Scanner kb = new Scanner(System.in);
        row = kb.nextInt();
        col = kb.nextInt();
        count += 1;
        table[row-1][col-1] = currentPlayer;
    }
    
    static void switchPlayer(){
        if(currentPlayer == 'X'){
            currentPlayer ='O';
        }else{
            currentPlayer = 'X';
        }
    }
    
    static boolean ifWin() {
        for (int i = 0; i < 3; i++) {
            if (table[i][0] == currentPlayer && table[i][1] == currentPlayer && table[i][2] == currentPlayer) {
                return true; 
            }
            if (table[0][i] == currentPlayer && table[1][i] == currentPlayer && table[2][i] == currentPlayer) {
                return true; 
            }
        }
        if (table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer) {
            return true; 
        }
        else if (table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer) {
            return true; 
        }
        return false;
    }
    
    static boolean ifDraw(){
        if (count == 9){
            return true;
        }
        return false;
    }
    
    static void printWin(){
        System.out.println("Player "+currentPlayer+" WIN!!!!!");
    }
    
    static void printDraw(){
        System.out.println("Draw!!!!!");
    }
    
    public static void main(String[] args) {
        printWelcome();
        while(true){
            printTable();
            printTurn();
            printMove();
            
            if (ifWin()){
                printTable();
                printWin();
                break;
            }
            
            if (ifDraw()){
                printTable();
                printDraw();
                break;
            }
            
            switchPlayer();
        }
    }
}
